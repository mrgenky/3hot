FROM php:8.0.13-apache-buster

RUN \
    apt update && \
    apt install -y ssl-cert msmtp msmtp-mta bsd-mailx && \
    docker-php-ext-install pdo pdo_mysql && \
    pecl install xdebug-3.0.4

RUN \
    a2enmod ssl rewrite && \
    a2ensite default-ssl && \
    docker-php-ext-enable xdebug

# copy needed files for production
COPY ./www /var/www/html
COPY ./node_modules /var/www/html/node_modules
COPY ./etc/apache2/sites-available /etc/apache2/sites-available
COPY ./etc/php/conf.d/sendmail.ini /usr/local/etc/php/conf.d/sendmail.ini
COPY ./etc/msmtprc /etc/msmtprc
