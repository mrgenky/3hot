import {store} from './utils/store.js';

const socket = new WebSocket("wss://ws.hothothot.dog:9502/");

socket.onopen = () => {
    setTimeout(function () {
        if (socket.readyState === 1) {
            socket.send("coucou !");
            console.log("Connexion établie");
        } else {
            console.log("état : " + socket.readyState);
        }
    }, 5000)
}

socket.onmessage = (event) => {
    try {
        const data = JSON.parse(event.data);
        store.update('interior.current', data.capteurs[0].Valeur);
        store.update('exterior.current', data.capteurs[1].Valeur);
    } catch (error) {
        // malformed data
        console.error(error);
    }
}

if ('serviceWorker' in navigator) {
    navigator.serviceWorker
        .register('/sw.js', {scope: '/'})
        .then(function (reg) {

            if (reg.installing) {
                console.log('Service worker installing');
            } else if (reg.waiting) {
                console.log('Service worker installed');
            } else if (reg.active) {
                console.log('Service worker active');
            }

        }).catch((error) => {
        // registration failed
        console.log('Registration failed with ' + error);
    });

}
