export const computeAlert = async (value) => {
    let alerteExt = "Aucune indication disponible.";

    try {
        const data = await fetch('/assets/json/alertes.json');
        const json = await data.json();

        if (value > 35) {
            alerteExt = json.alertes[0];
        } else if (value < 0) {
            alerteExt = json.alertes[1];
        }

    } catch (err) {
        console.error(err);
    }

    return alerteExt;
};
