class StoreObserver {
    callback = undefined;

    constructor(callback) {
        this.callback = callback;
    }

    notify(value) {
        this.callback(value);
    }

}

class StoreValue {
    /**
     * The current value
     * @type {any}
     */
    value = undefined;

    /**
     * @type {[StoreObserver]}
     */
    observers = [];

    setValue(newValue) {
        this.value = newValue;
        this.observers.forEach((observer) => {
            observer.notify(this.value);
        });
    }

    addEventListener(callback) {
        this.observers.push(new StoreObserver(callback));
    }

}

class Store {
    map = {};
    storage = undefined;

    constructor(storage) {
        this.storage = storage;
    }

    register(name) {
        this.map[name] = new StoreValue();
    }

    load() {
        Object.entries(this.map).forEach(([key, storeValue]) => {
            const decoded = JSON.parse(this.storage.getItem(key));
            storeValue.setValue(decoded);
        });
    }

    persist() {
        Object.entries(this.map).forEach(([key, storeValue]) => {
            this.storage.setItem(key, JSON.stringify(storeValue.value));
        });
    }

    update(key, value) {
        this.map[key].setValue(value);
    }

    addEventListener(key, callback) {
        this.map[key].addEventListener(callback);
    }
}

export const store = new Store(window.localStorage);

store.register('interior.current');
store.register('interior.minimum');
store.register('interior.maximum');
store.register('exterior.current');
store.register('exterior.minimum');
store.register('exterior.maximum');

store.load();
