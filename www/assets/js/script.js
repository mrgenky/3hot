import {makeChartConfig} from "./utils/chart.js";
import {store} from "./utils/store.js";
import {computeAlert} from "./utils/alert.js";

//Activation popovers
let popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
let popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
    return new bootstrap.Popover(popoverTriggerEl)
})


store.addEventListener('interior.current', async (value) => {
    document.getElementById("int-current").innerHTML = value + "°C";
    const popoverInt = new bootstrap.Popover(document.querySelector('#alerte-int'), {
        content: await computeAlert(value),
        trigger: 'focus'
    });
    popoverInt.show();
});

store.addEventListener('exterior.current', async (value) => {
    document.getElementById("ext-current").innerHTML = value + "°C";
    const popoverExt = new bootstrap.Popover(document.querySelector('#alerte-ext'), {
        content: await computeAlert(value),
        trigger: 'focus'
    });
    popoverExt.show();
});

store.addEventListener('interior.minimum', (value) => {
    document.getElementById("int-min").innerText = value + "°C";
});

store.addEventListener('interior.maximum', (value) => {
    document.getElementById("int-max").innerText = value + "°C";
});

store.addEventListener('exterior.minimum', (value) => {
    document.getElementById("ext-min").innerText = value + "°C";
});

store.addEventListener('exterior.maximum', (value) => {
    document.getElementById("ext-max").innerText = value + "°C";
});

//Exploitation fichier températures
fetch('/assets/json/temperatures.json')
    .then(data => data.json())
    .then((data) => {

        store.update('interior.minimum', data.tempInt.min);
        store.update('interior.maximum', data.tempInt.max);

        store.update('exterior.minimum', data.tempExt.min);
        store.update('exterior.maximum', data.tempExt.max);

        const moyInt = Object.values(data.tempInt.moy);
        const moyExt = Object.values(data.tempExt.moy);

        //Configuration du graph
        const ctx = document.getElementById('myChart');
        const myChart = new Chart(ctx, makeChartConfig(moyInt, moyExt));
    });

window.addEventListener('unload', () => {
    store.persist();
});
