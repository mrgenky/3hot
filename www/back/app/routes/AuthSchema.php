<?php


namespace app\routes;


use app\exceptions\AuthentificationException;
use app\exceptions\ConstraintException;
use app\exceptions\IllegalStateException;
use app\factories\EndUserFactory;
use app\forms\EditUserForm;
use app\persistors\PagePersistor;
use app\Repository\Forgotten_passwordRepository;
use app\Repository\RoleRepository;
use app\Repository\UserRepository;
use app\Tables\Forgotten_password;
use app\Tables\User;
use app\utils\Auth;
use app\utils\Messages;
use app\utils\Moment;
use app\views\LoginView;
use app\views\SignUpView;
use DateTime;
use rotor\Exact;
use rotor\Get;
use rotor\Post;
use viewer\View;

trait AuthSchema
{
    #[Exact]
    #[Get('/login')]
    function logIn() {
        if (!empty(Auth::getCurrentUser())) {
            \header('Location: /');
            return;
        }

        echo new LoginView();
    }

    function handleLogInFailure($error) {
        \http_response_code(400);

        echo new LoginView([
            'error' => $error
        ]);
    }

    #[Exact]
    #[Post('/login')]
    function postLogIn() {
        if(
            empty($_POST['username']) ||
            empty($_POST['password'])
        ) {
            $this->handleLogInFailure();
            return;
        }

        try {
            $user = Auth::authenticate($_POST['username'], $_POST['password']);

            Auth::logIn($user);

            Messages::$sharedInstance->add('success', 'Bienvenue');
            \header('Location: /');

        } catch (IllegalStateException $e) {
            $this->handleLogInFailure('Votre session a été bloquée pour des raisons de sécurité.');

        } catch (AuthentificationException $e) {
            $this->handleLogInFailure('Identifiants invalides.');

        }

    }


    #[Exact]
    #[Get('/signup')]
    function getSignUp() {
        if (!empty(Auth::getCurrentUser())) {
            \header('Location: /');
            return;
        }

        echo new SignUpView();
    }

    #[Exact]
    #[Post('/signup')]
    function postSignUp() {

        $form = new EditUserForm(
            $_POST['username'],
            $_POST['email'],
            $_POST['password'],
            $_POST['password-confirm']
        );

        if (!$form->isValid()) {
            $this->handleSignUpFailure('Veuillez remplir le formulaire et vérifier que les mots de passe sont égaux.');
            return;
        }

        try {
            $user = (new EndUserFactory())->create($_POST['username'], $_POST['password'], $_POST['email']);
        } catch (IllegalStateException $e) {
            $this->handleSignUpFailure('Le service de création d\'utilisateur est en panne.');
            return;
        }

        try {
            (new PagePersistor())->persist($user);
        } catch (ConstraintException $e) {
            $this->handleSignUpFailure('Un utilisateur possède déjà le même pseudo ou le même email.');
            return;
        }

        // Created
        \header('Location: /login');
    }

    #[Exact]
    #[Get('/logout')]
    public function logOut() {
        $user = Auth::getCurrentUser();

        if (!empty($user)) {
            Auth::logOut($user);
        }

        \header('Location: /');
    }

    private function handleSignUpFailure($error) {
        \http_response_code(400);

        echo new SignUpView([
            'error' => $error,
        ]);
    }

    public function showRecoverAccountForm() {
        echo View::fromFile('views/Layout.php', [
            'body' => View::fromFile('views/Recuperation.php')
        ]);
    }

    #[Get('/recover-account/')]
    public function handleGetRecoverAcount() {
        $this->showRecoverAccountForm();
    }

    #[Post('/recover-account/')]
    public function handleRecoverAccountRequest() {
        if (empty($_POST['email'])) {
            throw new \Exception('Empty mail');
        }

        $userRepository = new UserRepository();
        $user = $userRepository->findBy([
            'email' => $_POST['email'],
        ]);

        if (empty($user)) {
            throw new \Exception('User not found');
        }

        $token = Auth::makeToken();
        $expirationDate = Auth::makeExpirationDate();

        // TODO refactor
        $publicHost = 'http://localhost:3000';
        $appName = '3hot';
        $preferredDateTimeFormat = 'Y-m-d H:i:s';
        $date = \date($preferredDateTimeFormat);
        \mail($user->getEmail(), 'Reset your password', <<<EOF
Hello {$user->getName()},

Someone requested to reset your password at ${date}.

Here is the link to reset your password: {$publicHost}/reset-password/${token}.

The above link will expire in an hour.

Best regards,

{$appName}

EOF
);

        $forgottenPassword = new Forgotten_password();
        $forgottenPassword->setUser($user);
        $forgottenPassword->setToken($token);
        $forgottenPassword->setExpire_at(Moment::formatTimeStamp($expirationDate));

        $forgottenPasswordRepo = new Forgotten_passwordRepository();
        $forgottenPasswordRepo->persist($forgottenPassword);

        $this->showRecoverAccountForm();
    }

    #[Get('/reset-password/(\w+)')]
    public function handleResetPasswordGetRequest($params) {
        [$token] = $params;

        if (empty($token)) {
            throw new \ValueError('Missing token.');
        }

        $forgottenPasswordRepo = new Forgotten_passwordRepository();

        $forgottenPassword = $forgottenPasswordRepo->findBy([
            'token' => $token,
        ]);

        if (empty($forgottenPassword)
            || ((new \DateTime($forgottenPassword->getExpire_at()))->getTimestamp() < (new \DateTime())->getTimestamp())) {
            throw new AuthentificationException('Invalid token');
        }

        echo View::fromFile('views/Layout.php', [
            'body' => View::fromFile('views/ChangementMdp.php')
        ]);
    }

    #[Post('/reset-password/(\w+)')]
    public function handleResetPasswordPostRequest($params) {
        [$token] = $params;

        if (empty($token)) {
            throw new \ValueError('Missing token.');
        }

        if (
            empty($_POST['password'])
            || !Auth::isPasswordValid($_POST['password'])
        ) {
            throw new \ValueError('Invalid form');
        }

        $forgottenPasswordRepo = new Forgotten_passwordRepository();

        $forgottenPassword = $forgottenPasswordRepo->findBy([
            'token' => $token,
        ]);

        if (empty($forgottenPassword)
            || ((new \DateTime($forgottenPassword->getExpire_at()))->getTimestamp() < (new \DateTime())->getTimestamp())) {
            throw new AuthentificationException('Invalid token');
        }

        // make token expire now
        $forgottenPassword->setExpire_at(Moment::formatTimeStamp(new \DateTime()));
        $forgottenPasswordRepo->persist($forgottenPassword);

        // get user and reset his password!
        $user = $forgottenPassword->getUser();
        $hash = password_hash($_POST['password'], PASSWORD_BCRYPT);

        $user->setHash($hash);

        $userRepository = new UserRepository();
        $userRepository->persist($user);

        $_SESSION['success'] = 'Mot de passe réinitialisé avec succès !';
        \header('Location: /');
    }
}