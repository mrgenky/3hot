<?php

namespace app\routes;


use app\routes\admin\AdminSchema;
use app\routes\admin\AdminPageSchema;
use app\routes\admin\AdminSectionSchema;
use app\routes\admin\AdminUserSchema;
use app\views\CompteView;
use app\views\IndexView;
use rotor\Exact;
use rotor\Get;
use rotor\Fallback;
use viewer\View;

class MainSchema {

    use AdminSchema;
    use AdminUserSchema;
    use AdminPageSchema;
    use AdminSectionSchema;
    use AuthSchema;
    use DocumentationSchema;

    #[Exact]
    #[Get('/')]
    function index() {
        echo new IndexView();
    }

    #[Get('/test')]
    function test() {
        $roleRepo = new \app\Repository\RoleRepository();
        $role = $roleRepo->find(1);
        $role->setName("updated user");
        $roleRepo->persist($role);
    }


    #[Exact]
    #[Get('/compte')]
    function compte() {
        try {
            echo new CompteView();
        } catch (\ValueError $e) {
            $this->fallback();
        }
    }

    #[Fallback]
    function fallback() {
        http_response_code(404);
        echo View::fromFile('views/errors/404.php');
    }
}
