<?php

namespace app\routes;


use app\Repository\PageRepository;
use app\Repository\SectionRepository;
use app\views\docs\IndexView;
use app\views\docs\PageDetailsView;
use app\views\docs\SectionDetailsView;
use rotor\Exact;
use rotor\Get;
use viewer\View;

trait DocumentationSchema {

    #[Exact]
    #[Get('/documentation')]
    public function documentationIndex() {

        $sectionRepository = new SectionRepository();
        $sections = $sectionRepository->findAll();

        echo new IndexView($sections);
    }

    #[Get('/documentation/page/(\d+)')]
    public function documentationPage($params) {
        [$id] = $params;

        $pageRepository = new PageRepository();
        $page = $pageRepository->find($id);

        $sectionRepository = new SectionRepository();
        $sections = $sectionRepository->findAll();

        if (empty($page)) {
            return $this->fallback();
        }

        echo new PageDetailsView($page, $sections);
    }

    #[Get('/documentation/section/(\d+)')]
    public function documentationSection($params) {
        [$id] = $params;

        $sectionRepository = new SectionRepository();
        $sections = $sectionRepository->findAll();
        $section = $sectionRepository->find($id);

        if (empty($section)) {
            return $this->fallback();
        }

        echo new SectionDetailsView($section, $sections);
    }
}
