<?php


namespace app\routes\admin;


use app\exceptions\ConstraintException;
use app\exceptions\IllegalStateException;
use app\factories\EndUserFactory;
use app\forms\CreateUserForm;
use app\forms\EditUserForm;
use app\persistors\UserPersistor;
use app\Repository\UserRepository;
use app\utils\Auth;
use app\utils\Messages;
use app\views\AddUserView;
use app\views\EditUserView;
use rotor\Exact;
use rotor\Get;
use rotor\Post;
use viewer\View;

trait AdminUserSchema
{

    #[Get('/admin/user/edit/(\d+)')]
    function handleGetEditUser($params) {
        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        [$id] = $params;

        $userRepository = new UserRepository();
        $user = $userRepository->find($id);

        echo new EditUserView($user);
    }

    #[Post('/admin/user/edit/(\d+)')]
    function handlePostEditUser($params) {
        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        [$id] = $params;

        $form = new EditUserForm(
            $_POST['name'],
            $_POST['email']
        );

        if (!$form->isValid()) {
            Messages::$sharedInstance->add('error', 'Formulaire invalide.');
            $this->handleGetEditUser($params);
            return;
        }

        $userRepository = new UserRepository();
        $user = $userRepository->find($id);
        $user->setName($_POST['name']);
        $user->setEmail($_POST['email']);

        try {
            (new UserPersistor())->persist($user);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Impossible de dupliquer le nom ou le mot de passe.');
            $this->handleGetEditUser($params);
            return;
        }

        Messages::$sharedInstance->add('success', 'Utilisateur modifié avec succès');
        \header('Location: /admin');
    }

    #[Exact]
    #[Get('/admin/user/add')]
    function addUser() {

        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        echo new AddUserView();
    }

    #[Exact]
    #[Post('/admin/user/add')]
    function handlePostAddUser() {

        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        $form = new CreateUserForm(
            $_POST['username'],
            $_POST['email'],
            $_POST['password'],
            $_POST['password-confirm']
        );

        if (!$form->isValid()) {
            Messages::$sharedInstance->add('error', 'Veuillez remplir correctement le formulaire.');
            $this->addUser();
            return;
        }

        try {
            $user = (new EndUserFactory())->create($_POST['username'], $_POST['password'], $_POST['email']);
        } catch (IllegalStateException $e) {
            Messages::$sharedInstance->add('error', 'Ce service est en panne.');
            $this->addUser();
            return;
        }

        try {
            (new UserPersistor())->persist($user);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Impossible de dupliquer le nom out le mot de passe.');
            $this->addUser();
            return;
        }

        \header('Location: /admin');
    }


    #[Exact]
    #[Post('/admin/user/delete')]
    function deleteUser() {

        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        $ids = $_POST['ids'];

        if (empty($ids)) {
            \header('Location: /admin');
            Messages::$sharedInstance->add('error', 'Veuillez remplir le formulaire.');
            return;
        }

        $userRepository = new UserRepository();

        foreach($ids as $id) {
            $user = $userRepository->find($id);

            if (Auth::getCurrentUser()->getID() == $user->getID()) {
                Messages::$sharedInstance->add('error', 'Attention, vous ne pouvez pas supprimer votre propre compte!');
                continue;
            }

            try {
                $userRepository->delete($user);
            } catch (\ErrorException $e) {
                Messages::$sharedInstance->add('error', 'Un problème est survenu.');
            }
        }

        \header('Location: /admin');
    }

}