<?php


namespace app\routes\admin;


use app\exceptions\ConstraintException;
use app\factories\DocumentationPageFactory;
use app\forms\CreatePageForm;
use app\persistors\PagePersistor;
use app\Repository\PageRepository;
use app\Repository\SectionRepository;
use app\utils\Messages;
use app\views\AddPageView;
use app\views\EditPageView;
use rotor\Exact;
use rotor\Get;
use rotor\Post;

trait AdminPageSchema
{

    #[Exact]
    #[Get('/admin/page/add')]
    function addPage() {

        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        $sectionRepository = new SectionRepository();
        $sections = $sectionRepository->findAll();

        echo new AddPageView($sections);
    }


    #[Exact]
    #[Post('/admin/page/add')]
    function handlePostAddPage() {

        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        $form = new CreatePageForm(
            $_POST['title'],
            $_POST['content'],
            $_POST['section']
        );

        if (!$form->isValid()) {
            Messages::$sharedInstance->add('error', 'Veuillez correctement remplir le formulaire');
            $this->addPage();
            return;
        }

        try {
            $page = (new DocumentationPageFactory())->create(null, $_POST['title'], $_POST['content'], $_POST['section']);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Section non reconnue.');
            $this->addPage();
            return;
        }

        try {
            (new PagePersistor())->persist($page);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Une autre page possède déjà le même titre.');
            $this->addPage();
            return;
        }

        \header('Location: /admin');
    }


    #[Get('/admin/page/edit/(\d+)')]
    function editPage($params) {

        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        [$id] = $params;

        $pageRepo = new PageRepository();
        $page = $pageRepo->find($id);

        $sectionRepository = new SectionRepository();
        $sections = $sectionRepository->findAll();

        echo new EditPageView($page, $sections);
    }


    #[Post('/admin/page/edit/(\d+)')]
    function handlePostEditPage($params) {

        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        $form = new CreatePageForm(
            $_POST['title'],
            $_POST['content'],
            $_POST['section']
        );

        if (!$form->isValid()) {
            Messages::$sharedInstance->add('error', 'Veuillez correctement remplir le formulaire');
            $this->editPage($params);
            return;
        }

        [$id] = $params;

        try {
            $page = (new DocumentationPageFactory())->create($id, $_POST['title'], $_POST['content'], $_POST['section']);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Section ou page non reconnue.');
            $this->editPage($params);
            return;
        }

        try {
            (new PagePersistor())->persist($page);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Une autre page possède déjà le même titre.');
            $this->editPage($params);
            return;
        }

        \header('Location: /admin');
    }


    #[Exact]
    #[Post('/admin/page/delete')]
    public function deletePage() {
        $ids = $_POST['ids'];

        $pageRepository = new PageRepository();

        foreach ($ids as $id) {
            $page = $pageRepository->find($id);

            if (empty($page)) {
                Messages::$sharedInstance->add('error', 'Une erreur est survenue.');
                continue;
            }

            $pageRepository->delete($page);
        }

        \header('Location: /admin');
    }
}