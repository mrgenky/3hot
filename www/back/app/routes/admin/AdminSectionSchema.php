<?php


namespace app\routes\admin;


use app\exceptions\ConstraintException;
use app\factories\DocumentationSectionFactory;
use app\forms\SectionForm;
use app\persistors\SectionPersistor;
use app\Repository\PageRepository;
use app\Repository\SectionRepository;
use app\utils\Messages;
use app\views\AddSectionView;
use app\views\EditSectionView;
use rotor\Exact;
use rotor\Get;
use rotor\Post;

trait AdminSectionSchema
{

    #[Exact]
    #[Get('/admin/section/add')]
    function addSection() {

        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        echo new AddSectionView();
    }


    #[Exact]
    #[Post('/admin/section/add')]
    function handlePostAddSection() {

        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        $form = new SectionForm(
            $_POST['name'],
        );

        if (!$form->isValid()) {
            Messages::$sharedInstance->add('error', 'Veuillez correctement remplir le formulaire');
            $this->addSection();
            return;
        }

        try {
            $section = (new DocumentationSectionFactory())->create(null, $_POST['name']);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Section non reconnue.');
            $this->addSection();
            return;
        }

        try {
            (new SectionPersistor())->persist($section);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Une autre section possède déjà le même nom.');
            $this->addSection();
            return;
        }

        \header('Location: /admin');
    }


    #[Get('/admin/section/edit/(\d+)')]
    function editSection($params) {

        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        [$id] = $params;

        $sectionRepository = new SectionRepository();
        $section = $sectionRepository->find($id);

        echo new EditSectionView($section);
    }


    #[Post('/admin/section/edit/(\d+)')]
    function handlePostEditSection($params) {

        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        $form = new SectionForm(
            $_POST['name'],
        );

        if (!$form->isValid()) {
            Messages::$sharedInstance->add('error', 'Veuillez correctement remplir le formulaire');
            $this->editSection($params);
            return;
        }

        [$id] = $params;

        try {
            $section = (new DocumentationSectionFactory())->create($id, $_POST['name']);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Section non reconnue.');
            $this->editSection($params);
            return;
        }

        try {
            (new SectionPersistor())->persist($section);
        } catch (ConstraintException $e) {
            Messages::$sharedInstance->add('error', 'Une autre section possède déjà le même nom.');
            $this->editSection($params);
            return;
        }

        \header('Location: /admin');
    }


    #[Exact]
    #[Post('/admin/section/delete')]
    public function deleteSection() {
        $ids = $_POST['ids'];

        $sectionRepository = new SectionRepository();

        foreach ($ids as $id) {
            $page = $sectionRepository->find($id);

            if (empty($page)) {
                Messages::$sharedInstance->add('error', 'Une erreur est survenue.');
                continue;
            }

            try {
                $sectionRepository->delete($page);
            } catch (\PDOException $e) {
                Messages::$sharedInstance->add('error', 'Impossible de supprimer une section contenant des pages.');
                continue;
            }
        }

        \header('Location: /admin');
    }

}