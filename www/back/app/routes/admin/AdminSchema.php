<?php

namespace app\routes\admin;


use app\Repository\PageRepository;
use app\Repository\SectionRepository;
use app\Repository\UserRepository;
use app\Tables\User;
use app\utils\Auth;
use app\views\AdminDashboardView;
use rotor\Exact;
use rotor\Get;

trait AdminSchema {

    private function isAuthorized() {
        $user = Auth::getCurrentUser();

        return (
            !empty($user)
            && $user->getRole()->getName() === 'ADMIN'
        );
    }

    #[Exact]
    #[Get('/admin')]
    function adminIndex() {

        if (!$this->isAuthorized()) {
            return $this->fallback();
        }

        $userRepository = new UserRepository();
        $users = $userRepository->findAll();

        $pageRepository = new PageRepository();
        $pages = $pageRepository->findAll();

        $sectionRepository = new SectionRepository();
        $sections = $sectionRepository->findAll();

        echo new AdminDashboardView($users, $sections, $pages);
    }

}
