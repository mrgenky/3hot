<?php


namespace app\factories;


use app\Tables\Section;

interface SectionFactory
{

    public function create(?int $id, string $name): Section;

}