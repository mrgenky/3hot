<?php


namespace app\factories;


use app\Tables\User;

interface UserFactory
{
    public function create(string $username, string $password, string $email): User;

}