<?php

namespace app\views;

use viewer\View;

class AddPageView extends LayoutView
{
    public function __construct(array $sections, $params = array())
    {
        parent::__construct(
        'Ajouter une page',
            View::fromFile('views/backoffice/AddPage.php', [
                'sections' => $sections,
            ]),
            $params
        );
    }
}