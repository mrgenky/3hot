<?php


namespace app\views\docs;


use app\Tables\Page;
use viewer\View;

class PageDetailsView extends View
{

    public function __construct(Page $page, array $sections)
    {
        parent::__construct('views/Layout.php', [
            'h1' => 'Documentation / ' . $page->getTitle(),
            'body' => View::fromFile('views/docs/PageDetails.php', [
                'page' => $page,
                'navigation' => new NavigationView($sections),
            ]),
        ]);
    }

}