<?php

namespace app\views;

use viewer\View;

class AddUserView extends LayoutView
{
    public function __construct($params = [])
    {
        parent::__construct(
            'Ajouter un utilisateur',
            View::fromFile('views/backoffice/AddUser.php'),
            $params
        );
    }
}