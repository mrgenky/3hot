<?php


namespace app\views;


use viewer\View;

class AddSectionView extends LayoutView
{

    public function __construct() {
        parent::__construct(
            'Ajouter une section',
            View::fromFile('views/backoffice/AddSection.php')
        );
    }

}