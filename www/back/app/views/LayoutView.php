<?php


namespace app\views;


use app\utils\Messages;
use viewer\View;

class LayoutView extends \viewer\View
{
    public function __construct($h1, $body, $params = array())
    {
        parent::__construct('views/Layout.php', $params);

        $this->params['h1'] = $h1;
        $this->params['error'] = Messages::$sharedInstance->consume('error');
        $this->params['success'] = Messages::$sharedInstance->consume('success');
        $this->params['body'] = $body;
        $this->params['scripts'] = [
            'app.js',
            'script.js',
        ];
    }

}
