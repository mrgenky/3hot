<?php

namespace app\views;

use viewer\View;

class CompteView extends LayoutView
{
    public function __construct($params = array())
    {
        parent::__construct(
            'Mon compte',
            View::fromFile('views/Compte.php'),
            $params
        );
    }
}