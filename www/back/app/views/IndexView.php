<?php

namespace app\views;

use viewer\View;

class IndexView extends LayoutView
{
    public function __construct($params = array())
    {
        parent::__construct(
            'Acceuil',
            View::fromFile('views/Welcome.php'),
            $params
        );
    }
}