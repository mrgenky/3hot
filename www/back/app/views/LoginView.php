<?php

namespace app\views;

use viewer\View;

class LoginView extends LayoutView
{
    public function __construct($params = array())
    {
        parent::__construct(
            'Connexion',
            View::fromFile('views/Connexion.php', $params),
        );
    }
}