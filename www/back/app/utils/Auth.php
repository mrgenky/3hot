<?php


namespace app\utils;


use app\exceptions\AuthentificationException;
use app\exceptions\IllegalStateException;
use app\Repository\ConnectionRepository;
use app\Repository\UserRepository;
use app\Tables\Connection;
use app\Tables\User;

class Auth
{
    const USER_ID_KEY = 'user_id';
    const FAILED_AUTHENTICATION_COUNT_KEY = 'failed_authentication_count_key';
    const MAX_FAILED_AUTHENTICATION_COUNT = 3;

    /**
     * Numbers of characters for the token.
     * Must be a power of 2: 32, 64, 128, 256, and so on.
     */
    const TOKEN_LENGTH = 128;

    /**
     * The duration of a token in hours.
     */
    const TOKEN_DURATION = 1;

    static public function authenticate($username, $password) {
        if (
            !empty($_SESSION[self::FAILED_AUTHENTICATION_COUNT_KEY])
            && $_SESSION[self::FAILED_AUTHENTICATION_COUNT_KEY] === self::MAX_FAILED_AUTHENTICATION_COUNT
        ) {
            throw new IllegalStateException();
        }

        $userRepository = new UserRepository();

        $user = $userRepository->findBy([
            'name' => $username,
        ]);

        if (
            empty($user)
            || !password_verify($password, $user->getHash())
        ) {
            // increment failed auth count by 1
            if (empty($_SESSION[self::FAILED_AUTHENTICATION_COUNT_KEY])) {
                $_SESSION[self::FAILED_AUTHENTICATION_COUNT_KEY] = 1;
            } else {
                $_SESSION[self::FAILED_AUTHENTICATION_COUNT_KEY] += 1;
            }

            throw new AuthentificationException();
        }

        unset($_SESSION[self::FAILED_AUTHENTICATION_COUNT_KEY]);

        return $user;
   }

    static public function logIn($user) {
        $connectionRepo = new ConnectionRepository();

        $connection = new Connection();
        $connection->setUser($user);
        $connection->setConnected_at(
            Moment::formatTimeStamp(new \DateTime())
        );

        // grants a one hour session
        $connection->setDisconnected_at(
            Moment::formatTimeStamp(\DateTime::createFromFormat('U', \time() + 3600))
        );

        $connectionRepo->persist($connection);

        $_SESSION[self::USER_ID_KEY] = $user->getId();
    }

    static public function getCurrentUser() {
        if (empty($_SESSION[self::USER_ID_KEY]))
            return null;

        $userRepository = new UserRepository();
        return $userRepository->find($_SESSION[self::USER_ID_KEY]);
    }

    static public function logOut(User $user) {
        unset($_SESSION[self::USER_ID_KEY]);

        $connection = $user->getConnection();

        // FIXME this raises an undefined method `setDisconnected_at`
        $connection->setDisconnected_at(
            Moment::formatTimeStamp(new \DateTime())
        );

        $connection->persist();
    }

    static public function isUsernameValid($username) {
        return preg_match('/^[a-zA-Z0-9\-_]{4,20}$/', $username) === 1;
    }

    static public function isEmailValid(string $email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    static public function isPasswordValid(string $password) {
        return preg_match('/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/', $password) === 1;
    }

    static public function makeToken() {
        return \bin2hex(\random_bytes(self::TOKEN_LENGTH / 2));
    }

    public static function makeExpirationDate() {
        $date = new \DateTime();
        $modifier = self::TOKEN_DURATION . ' hour';
        $date->modify($modifier);
        return $date;
    }

}