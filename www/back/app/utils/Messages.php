<?php


namespace app\utils;


class Messages
{
    static public $sharedInstance;

    public function add(string $key, $value) {
        $_SESSION[$key] = $value;
    }

    public function consume(string $key) {
        $copy = $_SESSION[$key] ?? null;
        unset ($_SESSION[$key]);
        return $copy;
    }

}

Messages::$sharedInstance = new Messages();
