<?php


namespace app\forms;


use app\forms\fields\TextField;

class SectionForm extends Form
{

    public function __construct(
        string $name
    ) {
        parent::__construct([
            new TextField('name', $name, 3, 128),
        ]);
    }

}