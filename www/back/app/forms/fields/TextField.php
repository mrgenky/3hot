<?php


namespace app\forms\fields;


class TextField extends Field
{

    public function __construct(
        string $name,
        mixed $value,
        private int $minLength,
        private int $maxLength
    )
    {
        parent::__construct($name, $value);
    }

    public function isValid(): bool
    {
        return
               !empty($this->getValue())
            && strlen($this->getValue()) >= $this->minLength
            && strlen($this->getValue()) <= $this->maxLength;
    }
}