<?php


namespace app\forms\fields;


class PasswordField extends Field
{

    public function isValid(): bool
    {
        return \preg_match(
            '/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/',
            $this->getValue()
        ) === 1;
    }
}