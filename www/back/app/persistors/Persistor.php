<?php


namespace app\persistors;


/**
 * Interface Persistor
 * @template T
 * @package app\persistors
 */
interface Persistor
{

    /**
     * @param T $object
     */
    public function persist($object): void;

}