<?php


namespace app\persistors;


use app\exceptions\ConstraintException;
use app\Repository\UserRepository;
use app\Tables\User;

/**
 * Class UserPersistor
 * @implements Persistor<User>
 * @package app\persistors
 */
class UserPersistor implements Persistor
{

    /**
     * @inheritDoc
     * @param User $user
     */
    public function persist($user): void
    {
        $userRepository = new UserRepository();

        $userWithSameName = $userRepository->findOneBy([
            'name' => $user->getName(),
        ]);

        if ($userWithSameName && $userWithSameName->getID() !== $user->getID()) {
            throw new ConstraintException();
        }

        $userWithSameEmail = $userRepository->findOneBy([
            'email' => $user->getEmail(),
        ]);

        if ($userWithSameEmail && $userWithSameEmail->getID() !== $user->getID()) {
            throw new ConstraintException();
        }

        $userRepository->persist($user);
    }
}