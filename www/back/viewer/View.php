<?php


namespace viewer;


class View
{
    public function __construct(
        private $filename,
        protected $params = array()
    ) {}

    public static function fromFile(
        string $filename,
        array $params = array()
    ) {
        return new static($filename, $params);
    }

    public function render(): void {
        if (!file_exists($this->filename)) {
            throw new \ValueError(
                'View '
                . $this->filename
                . ' not found'
            );
        }

        extract($this->params);
        include $this->filename;
    }

    public function __toString() {
        ob_start();
        $this->render();
        return ob_get_clean();
    }
}