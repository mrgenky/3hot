<?php

namespace modelo\ORM\Manager;

interface DBManager
{
    public function create(String $table, array $values);
    public function read(String $table, array $values, array $criteres);
    public function update(String $table, array $values, array $criteres);
    public function delete(String $table, array $criteres);
    public function getTables();
    public function newTable(String $table, array $column);
    public function updateTable(String $table, array $column);
}