<?php

namespace modelo\ORM\Manager;
use modelo\Table\AbstractTable;
use PDO;
use Src\Tables\Formation;

class MySQLManager implements DBManager
{
    /**
     * @var PDO
     */
    private $pdo;

    public function __construct(String $host, String $dbName, String $user, String $password)
    {
        $dns = 'mysql:host=' . $host .  ':3306;dbname=' . $dbName . ';charset=UTF8';
        $this->pdo = new PDO($dns, $user, $password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @param String $table
     * @param array $values
     * @return void
     *
     * Créer un nouvel enregistrement dans une table MySQL
     */
    public function create(String $table, array $values) : bool
    {
        unset($values['id']);
        $req = 'INSERT INTO ' . $table . ' (';
        $i = 0;
        foreach ($values as $column => $value) {
            $req .= $column;
            if ($i + 1 < sizeof($values)) {
                $req .= ', ';
            } else {
                $req .= ')';
            }

            $i++;
        }
        $req .= ' VALUES (';
        $i = 0;
        foreach ($values as $column => $value) {
            $req .= ':' . $column;
            if ($i + 1 < sizeof($values)) {
                $req .= ', ';
            } else {
                $req .= ')';
            }
            $i++;
        }
        $query = $this->pdo->prepare($req);

        return $query->execute($values);
    }

    /**
     * @param String $table
     * @param array $criteres
     *
     * Permet de séléctionner un enregistrement grace à des critères
     * dans une table MySQL
     */
    public function read(String $table, array $values = [], array $criteres = []) : Array
    {
        if (empty($values)) {
            $req = 'SELECT * FROM ' . $table;
        } else {
            $req = 'SELECT ' . implode(", ", $values) . ' FROM ' . $table;
        }
        $req = $this->setWhere($req, $criteres);
        $attributs = [];
        foreach ($criteres as $colName => $critere) {
            $attributs['P' .$colName] = $critere[1];
        }
        $query = $this->pdo->prepare($req);
        $query->execute($attributs);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * @param String $table
     * @param array $values
     * @param array $criteres
     *
     * @return void
     *
     * Permet de mettre à jour un enregistrement à l'aide de critères
     * dans une table MySQL
     */
    public function update(String $table, array $values, array $criteres) : bool
    {
        $req = 'UPDATE ' . $table . ' SET';
        $req = $this->setWhere($this->setValues($req, $values), $criteres);
        /*
         * Si les valeurs à redéfinir font parties des critères de recherche (where)
         * On renomme le paramètre de recherche en ajoutant un P
         * => Pour différencier les paramètre de valeurs d'un paramètre de recherche pour une requête préparée
         */

        foreach ($criteres as $key => $critere)
        {
            $values += ['P' . $key => $critere[1]];
        }

        $query = $this->pdo->prepare($req);
        $result = $query->execute($values);

        return $result;
        // TODO: Implement update() method.
    }

    /**
     * Permet de récupérer la structure de la base de données configurée (table et colones)
     * @return array
     */
    public function getTables() : array
    {
        $query = $this->pdo->query("SHOW TABLES");
        $tables = $query->fetchAll(PDO::FETCH_ASSOC);
        $columList = [];
        foreach ($tables as $table) {
            foreach ($table as $tableName) {
                $query = $this->pdo->query("SHOW COLUMNS FROM " . $tableName);
                $columList[$tableName] = $query->fetchAll(PDO::FETCH_ASSOC);
            }
        }
        return $columList;
    }

    /**
     * Permet de créer une nouvelle table SQL dans la base de donnée configurée
     * @param String $tableName
     * @param array $column
     */
    public function newTable(String $tableName, array $column)
    {
        $req = 'CREATE TABLE ' . $tableName . '(' . "\n" . 'id INT PRIMARY KEY NOT NULL, ' . "\n";
        $fkFields = [];
        $i = 0;
        foreach ($column as $field) {
            ++$i;
            if (isset($field['relation']) && ($field['relation'] == 'manytomany' || $field['relation'] == 'manytoone')) {
                if ($field['relation'] == 'manytomany') {
                     $this->newTable($tableName . '_' . $field['fk_table'], [
                         ['name' => 'fk_' . $tableName, 'type' => 'int', 'primary' => true, 'fk' => true, 'fk_table' => $tableName],
                         ['name' => 'fk_' . $field['fk_table'], 'type' => 'int', 'primary' => true, 'fk' => true, 'fk_table' => $field['fk_table']]
                     ]);
                } else {
                    $this->updateTable($field['fk_table'], [
                        ['action' => 'create', 'name' => 'fk' . '_' . strtolower($tableName), 'type' => 'int', 'fk' => true, 'fk_table' => strtolower($tableName)]
                    ]);
                }
            } else {
                if ((isset($field['relation']) && ($field['relation'] == 'onetoone' || $field['relation'] == 'onetomany')) || isset($field['fk']) && $field['fk']) {
                    $field['name'] = 'fk_' . $field['fk_table'];
                    $fkFields[$field['name']] = $field['fk_table'];

                    if (isset($field['relation']) && $field['relation'] == 'onetoone') {
                         $this->updateTable($field['fk_table'], [
                             ['action' => 'create', 'name' => $field['name'], 'type' => 'int', 'null' => $field['null'], 'unique' => $field['unique'], 'fk' => true, 'fk_table' => strtolower($tableName)]
                         ]);
                    }
                }
                $req .= $field['name'] . ' ';

                if ($field['type'] === 'string') $field['type'] = 'varchar';
                elseif ($field['type'] === 'relation') $field['type'] = 'int';

                $req .= strtoupper($field['type']);

                if (isset($field['size']) && $field['size'] > 0) $req .= '(' . $field['size'] . ')';

                if (isset($field['primary']) && $field['primary']) $req .= ' PRIMARY KEY';

                if ($field['null'] === 'false') $req .= ' NOT NULL';

                if ($field['unique'] === 'true') $req .= ' UNIQUE';
            } //else
            if ($i < sizeof($column)) $req .= ', ' . "\n";
        } //foreach
        $req = $this->setFKConstraint($req, $fkFields);

        echo 'create : ';

        $this->pdo->exec($req);
    }

    /**
     * Permet de mettre à jour une table en base SQL
     * @param String $tableName nom de la table à mettre à jour
     * @param array $column format : [['action' => {'create', 'update', 'delete'}, 'name' => nom_colone, 'type' => {'string', 'text' 'int', 'DateTime', 'boolean', 'real'}, 'size' => 255, 'primary' => {true, false}, 'null' => {true, false}, 'unique' => {true, false}, 'relation' => {'onetoone', 'onetomany', 'manytoone', 'manytomany'}, 'fk' => {true, false}, 'fk_table' => 'table_name'], [...]]
     */
    public function updateTable(String $tableName, array $column)
    {
        $fkFields = [];
        $req = 'ALTER TABLE ' . $tableName . ' ';
        $i = 0;
        foreach ($column as $field) {
            ++$i;
            if (isset($field['relation']) && ($field['relation'] == 'manytoone' || $field['relation'] == 'manytomany')) {
                if ($field['relation'] == 'manytomany') {
                    $this->newTable($tableName . '_' . $field['fk_table'], [
                        ['name' => 'fk_' . $tableName, 'type' => 'int', 'primary' => true, 'fk' => true, 'fk_table' => $tableName],
                        ['name' => 'fk_' . $field['fk_table'], 'type' => 'int', 'primary' => true, 'fk' => true, 'fk_table' => $field['fk_table']]
                    ]);
                } else {
                    $this->updateTable($field['fk_table'], [
                        ['action' => 'create', 'name' => 'fk' . '_' . strtolower($tableName), 'type' => 'int', 'fk' => true, 'fk_table' => strtolower($tableName)]
                    ]);
                }
            } else {
                if ((isset($field['relation']) && ($field['relation'] == 'onetoone' || $field['relation'] == 'onetomany')) || isset($field['fk']) && $field['fk']) {
                    $field['name'] = 'fk_' . $field['fk_table'];
                    $fkFields[$field['name']] = $field['fk_table'];

                    if (isset($field['relation']) && $field['relation'] == 'onetoone') {
                        $this->updateTable($field['fk_table'], [
                            ['action' => 'create', 'name' => $field['name'], 'type' => 'int', 'null' => $field['null'], 'unique' => $field['unique'], 'fk' => true, 'fk_table' => strtolower($tableName)]
                        ]);
                    }
                }

                $req .= match ($column['action']) {
                    'create' => 'ADD ',
                    'update' => 'MODIFY ',
                    'delete' => 'DROP ',
                }; //match
                $req .= $column['name'] . ' ';

                if ($column['type'] === 'string') $column['type'] = 'varchar';
                $req .= $column['type'];

                if (isset($field['size'])) $req .= '(' . $field['size'] . ') ';
                else $req .= ' ';

                if (isset($field['primary']) && $field['primary']) $req .= 'PRIMARY KEY ';

                if (isset($field['null']) && !$field['null']) $req .= 'NOT NULL ';

                if (isset($field['unique']) && $field['unique']) $req .= 'UNIQUE ';
            } //else
            if ($i < sizeof($column)) $req .= ', ';
        } //foreach
        $req = $this->setFKConstraint($req, $fkFields, true);

        echo 'update : ';
        var_dump($req);

        $this->pdo->exec($req);
    }

    /**
     * Supprime un enregistrement d'une table en fonction de critères (where)
     * @param String $table
     * @param array $criteres
     * @return bool
     */
    public function delete(String $table, array $criteres) : bool
    {
        $req = 'DELETE FROM ' . $table;
        $req = $this->setWhere($req, $criteres);
        $parseCriteres = [];
        foreach ($criteres as $key => $critere) {
            $parseCriteres += ['P' . $key => $critere[1]];
        } //foreach
        $query = $this->pdo->prepare($req);
        return $query->execute($parseCriteres);
    }

    /************* HELPER METHODES *************/


    /**
     * @param String $request
     * @param array $columns
     * @return String
     *
     * Permet de définir la liste des colones / valeurs sur lequelles
     * intéragir dans une requette SQL en l'ajoutant cette liste à celle-ci
     *
     * La requête avec la liste des valeur est ensuite retournée.
     */
    private function setValues(String $request, array $columns) : String
    {
        $columns = array_keys($columns);

        foreach ($columns as $key => $column) {
            $request .= ' ' . $column . ' = :' . $column;

            if ($key + 1 < sizeof($columns)) {
                $request .= ', ';
            } //if
        } //foreach

        return $request;
    }

    /**
     * @param String $request
     * @param array $criteres
     * @return String
     *
     * Définit les conditions de recherches (where) d'une requête
     * en fournissant celle-ci et les critères
     * Revoie la requête avec WHERE
     */
    private function setWhere(String $request, array $criteres) : String
    {
        if (!empty($criteres))
        {
            $request .= ' WHERE ';
            $i = 0;
            foreach ($criteres as $colName => $critere) {
                $request .= $colName . ' ' . $critere[0] . ' ' . ':P' . $colName;
                if ($i > sizeof($criteres)) {
                    $request .= ' AND ';
                }
                $i++;
            }
        } //if
        return $request;
    }

    private function setFKConstraint (String $request, array $fkFields, bool $update = false) : string
    {
        if (!empty($fkFields)) {
            $i = 0;
            foreach ($fkFields as $key => $tableRef) {
                ++$i;
                $update ? $request .= ', ADD ' : $request .= '';
                $request .= ', FOREIGN KEY(' . $key . ') REFERENCES '. $tableRef . '(id)';
                if (sizeof($fkFields) == $i) {
                   $update ? $request .= ';' : $request .= ');';
                } else {
                    $request .= ', ' . "\n";
                } //else
            } //foreach
        } else {
           $update ? $request .= ';' : $request .= ');';
        } //else

        return $request;
    }
}