<?php

namespace modelo\ORM;

use modelo\Table\AbstractTable;

class ORM
{
    private $dbConnector;
    private static $orm = null;

    /**
     * @param DBConnector $dbConnector
     */
    private function __construct(DBConnector $dbConnector)
    {
        $this->dbConnector = $dbConnector;
    }

    /**
     * Méthode permettant de récupérer une instance unique de la class ORM
     * (singleton)
     * /!\/!\ A Appeler sans argument !
     * @param DBConnector|null $dbConnector
     * @return ORM
     */
    public static function getORM(?DBConnector $dbConnector = null)
    {
        if (is_null(self::$orm)) {
            self::$orm = new ORM($dbConnector);
        }
        return self::$orm;
    }

    /**
     * Permet de décripter le language intermediaire (orm) pour initiliser les vrais requêtes
     * selon le connecteur de BDD choisi
     * @param String $request
     * @param String $tableName
     * @return Array
     */
    public function buildRequest(String $request, String $tableName) : Array | bool
    {
        $tableName = lcfirst($tableName);
        //Formatage de la request (parse)
        $request = explode(" ", $request);
        foreach ($request as $key => $req) {
            if (empty($req)) {
                unset($request[$key]);
            }
        }

        //On traduit la requête
        $action = null;
        $column = [];
        $where = [];

        switch ($request[0]) {
            case "READ":
                $action = 'read';
                switch ($request[1]) {
                    case '*' :
                        $column = [];
                        if (isset($request[2]) && $request[2] == 'WHERE') {
                            $where = $this->setWhere($request, 2);
                        } //if
                        break;
                    default:
                        for ($i = 1; $i < sizeof($request); $i++)
                        {
                            if (!ctype_upper($request[$i]))
                            {
                                $column[] = explode(",", $request[$i])[0];
                            } else {
                                $where = $this->setWhere($request, $i);
                                break;
                            } //else
                        } //for
                } //switch
                break;
            case "PERSIST":
                $action = 'persist';
                for ($i = 1; $i < sizeof($request); $i++) {
                    $columnName = explode('_', $request[$i]);
                    if (isset($columnName[1])) $columnName = $columnName[1];
                    else $columnName = $columnName[0];
                    if (!ctype_upper($columnName) && ctype_alpha($columnName) && !ctype_alpha($request[$i + 1]) && (!ctype_upper($request[$i + 2]) )) {
                        $value = '';
                        if (substr($request[$i + 2], 0, 1) === '"') {
                            for ($j = $i + 2; $j < $request; $j++) {
                                $value .= $request[$j];

                                if (substr($request[$j], -1) === '"') {
                                    break;
                                }

                                $value .= ' ';
                            } //for
                        }
                        $value = explode('"', $value)[1];
                        if (is_numeric($value)) $value = (int) $value;
                        $column += [$request[$i] => $value];
                    }
                }
                break;
                //UPDATE titre = Mon titre AND description = ma super description WHERE id = 2
            case "UPDATE":
                $action = 'update';
                for ($i = 1; $i < sizeof($request); $i++) {
                    $columnName = explode('_', $request[$i]);
                    if (isset($columnName[1])) $columnName = $columnName[1];
                    else $columnName = $columnName[0];
                    if ($i + 2 < sizeof($request) && (!ctype_upper($columnName) && ctype_alpha($columnName)) && !ctype_alpha($request[$i + 1]) && !ctype_upper ($request[$i + 2]) ) {
                        $value = '';
                        if (str_starts_with($request[$i + 2], '"')) {
                            for ($j = $i + 2; $j < sizeof($request); $j++) {
                                $value .= $request[$j];

                                if (str_ends_with($request[$j], '"')) {
                                    break;
                                }
                                $value .= ' ';
                            }
                            $value = explode('"', $value)[1];
                        } else {
                            $value = $request[$i + 2];
                        }
                        $column += [$request[$i] => $value];

                    } elseif ($request[$i] === 'WHERE') {
                        $where = $this->setWhere($request, $i);
                    }
                }
                break;

            case "DELETE":
                $action = 'delete';
                if ($request[1] == 'WHERE') {
                    $where = $this->setWhere($request, 1);
                }
                break;

            case "NEW":
                switch ($request[1]) {
                    case "TABLE":
                        $action = 'new_table';
                        if ($request[2] === $tableName && $request[3] === 'FIELDS' && $request[4] === '(') {
                            $field = [];

                            for ($i = 5; $i < sizeof($request) - 1; $i++) {
                                if (ctype_upper($request[$i]) && $request[$i + 1] == '=' && (ctype_lower(explode(',', $request[$i + 2])[0]) || ctype_lower(explode(';', $request[$i + 2])[0]) || is_numeric(explode(';', $request[$i + 2])[0]))) {

                                    $value = explode(',', $request[$i + 2]);
                                    (sizeof($value) <= 1) ? $value = explode(';', $request[$i + 2])[0] : $value = $value[0];
                                    switch ($request[$i]) {
                                        case 'NAME':
                                            $field['name'] = $value;
                                            break;
                                        case 'TYPE':
                                            $field['type'] = $value;
                                            break;
                                        case 'NULL':
                                            $field['null'] = $value;
                                            break;
                                        case 'UNIQUE':
                                            $field['unique'] = $value;
                                            break;
                                        case 'SIZE':
                                            $field['size'] = $value;
                                            break;
                                        case 'RELATION':
                                            $field['relation'] = $value;
                                            break;
                                        case 'FKTABLE':
                                            $field['fk_table'] = $value;
                                            break;
                                    } //switch
                                } if(sizeof(explode(';', $request[$i])) > 1) {
                                    $column[] = $field;
                                }
                            } //for
                        } //if
                        break;
                } //switch
                break;
            default:
                $action = null;
        } //switch
        if ($action === 'read') {
            $result = $this->dbConnector->db()->read($tableName, $column, $where);
        }
        elseif ($action === 'persist') {
            $result = $this->dbConnector->db()->create($tableName, $column);
        }
        elseif ($action === 'update') {
            $result = $this->dbConnector->db()->update($tableName, $column, $where);
        }
        elseif ($action === 'delete') {
            $result = $this->dbConnector->db()->delete($tableName, $where);
        }
        elseif ($action === 'new_table') {
            $this->dbConnector->db()->newTable($tableName, $column);
            $result = [];
        } //elseif

        return $result;
    }

    /**
     * Permet d'importer la structure d'une base de donnée selon le connecteur choisit
     * méthode appelée par la commande "orm:database-import" après avoir configuré le fichier config.ini
     * @return array
     */
    public function importDATABASE() : array
    {
        return $this->dbConnector->db()->getTables();
    }

    private function setWhere(array $request, int $index) : array
    {
        $where = [];
        if ($request[$index] === 'WHERE') {
            for ($i = $index + 1; $i < sizeof($request); $i++) {
                $columnName = explode('_', $request[$i]);
                if (isset($columnName[1])) $columnName = $columnName[1];
                else $columnName = $columnName[0];
                if (($i + 2) < sizeof($request) && !ctype_upper($columnName) && ctype_alpha($columnName) && !ctype_alpha($request[$i + 1]) && $i + 2 < sizeof($request)) {
                    if (!ctype_alpha($request[$i + 1])) {
                        if (in_array($request[$i + 1], ['=', '<', '>'])) {
                            if (!ctype_upper($request[$i + 2])) {
                                $where += [$request[$i] => [$request[$i + 1], $request[$i + 2]]];
                            }
                        }
                    }
                }
            }
        }
        return $where;
    }

}