<?php

namespace modelo\ORM;

use modelo\Table\AbstractTable;

class Collection
{
    private $targetRepository;

    private array $collection;

    public function __construct(String $repository)
    {
        $this->targetRepository = new $repository();

        $this->collection = array();
    }

    public function add(AbstractTable $object) : void
    {
        if (get_class($object) == get_class($this->targetRepository->getTable())) {
            array_push($this->collection, $object);
        }
    }

    public function getAll() : array
    {
        return $this->collection;
    }

    public function get($id) : AbstractTable
    {
        return $this->collection[$id];
    }
}