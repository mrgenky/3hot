<?php

namespace modelo\ORM;

use modelo\ORM\Manager\DBManager;
use modelo\ORM\Manager\MySQLManager;

class DBConnector
{
    public const MYSQL = 'mysql';

    /**
     * @var DBManager
     */
    private $dbConnector;

    private $typeDataBase;
    private $host;
    private $dbName;
    private $user;
    private $password;

    public function __construct($typeDataBase, $host, $dbName, $user, $password)
    {
        $this->typeDataBase = $typeDataBase;
        $this->host = $host;
        $this->dbName = $dbName;
        $this->user = $user;
        $this->password = $password;

        if ($typeDataBase === self::MYSQL)
        {
            $this->setMySQL();
        }
    }

    private function setMySQL()
    {
        $this->dbConnector = new MySQLManager($this->host, $this->dbName, $this->user, $this->password);
    }

    /**
     * @return mixed
     * Permet de accèder à la bonne base de donnée
     */
    public function db()
    {
        return $this->dbConnector;
    }
}