<?php

namespace modelo\Table;

abstract class AbstractTable
{
    private static $lastID = 0;

    private $id;

    /*private $created_at;

    private $updated_at;*/

    public function getID() : int
    {
        return $this->id ?? 0;
    }

    public function setID(int $id = 0)
    {
        if ($id > 0) {
            $this->id = $id;
        } else {
            $this->id = ++self::$lastID;
        }
    }

    /*public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt(String $createdAt)
    {
        $this->created_at = $createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(String $updatedAt)
    {
        $this->updated_at = $updatedAt;
    }*/

}