<?php

use modelo\ORM\DBConnector;
use modelo\ORM\ORM;

[
    'DB_TYPE' => $dbType,
    'DB_NAME' => $dbName,
    'DB_USER' => $dbUser,
    'DB_PASSWORD' => $dbPassword,
    'DB_HOST' => $dbHost,
    'DB_STATE' => $dbState,

] = $_ENV;

if ($dbState) {
    
    $orm = ORM::getORM(new DBConnector($dbType, $dbHost, $dbName, $dbUser, $dbPassword));
}
