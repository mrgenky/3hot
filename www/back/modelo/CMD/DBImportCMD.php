<?php

namespace modelo\CMD;

use modelo\ORM\ORM;

class DBImportCMD extends AbstractCMD
{
    public function __construct()
    {
        $this->cmdName = "orm:database-import";
        //Définit une aide qui s'affiche lorsque l'on tape -help en argument
        $this->help = 'Commande permettant de générer un contrôleur basique et une vue par défaut';
    }

    public function run()
    {
        $orm = ORM::getORM();
        if (!is_null($orm)) {
            echo "Importation de la base de données en cours... \n";

            //Importation de la structure de la base de donnée (Tables + champs)
            $tables = $orm->importDATABASE();
            $manyToManyFields = [];
            //Récupération des Tables ManyToMany
            foreach ($tables as $tableName => $fields) {
                if (sizeof(explode("_", $tableName)) > 1) {
                    $compositeTableName = explode("_", $tableName);
                    $isManyToMany = true;
                    foreach ($compositeTableName as $distantTable) {
                        if (!array_key_exists($distantTable, $tables)) {
                            $isManyToMany = false;
                            break;
                        } //if
                    } //foreach
                    if ($isManyToMany) {
                        $manyToManyFields[$tableName] = $fields;
                    } //if
                } //if
            } //foreach
            /**
             * Génération des classes "Table" pour chaque table de la base
             */
            foreach ($tables as $tableName => $fields) {
                if (array_key_exists($tableName, $manyToManyFields)) {
                    continue;
                }

                $tableClass = '<?php
namespace app\Tables;

use modelo\Table\AbstractTable;
use modelo\ORM\Collection;' . "\n";
                foreach ($manyToManyFields as $tableNameManyMany => $manyToManyField) {
                    $parseTableNameManyToMany = explode("_", $tableNameManyMany);
                    if (in_array($tableName, $parseTableNameManyToMany)) {
                        foreach($parseTableNameManyToMany as $nameTableManyTomany) {
                            if ($nameTableManyTomany != $tableName) {
                                $tableClass .= 'use Src\Repository\\' . ucfirst($nameTableManyTomany) . 'Repository;' . "\n";
                            } //if
                        } //foreach
                    } //if
                } //foreach
                $tableClass .= 'use DateTime;

class ' . ucfirst($tableName) . ' extends AbstractTable
{
';
                $setter = [];
                foreach ($fields as $field) {
                    if ($field['Key'] != 'PRI' && $field['Key'] != "MUL") {
                        $tableClass .= '
    private ';
                        if ($field["Null"] == "YES") $tableClass .= '?';

                        if ((strpos($field["Type"], "varchar") !== false) || (strpos($field["Type"], "text") !== false || strpos($field["Type"], "timestamp") !== false)) {
                            $tableClass .= 'String $';
                            $setter[$field['Field']] = ['type' => 'String'];
                            if (strpos($field["Type"], "varchar") !== false) {
                                $setter[$field['Field']] += ['length' => substr($field['Type'], 8, 3)];
                            } //if
                            $setter[$field['Field']] += ['null' => $field['Null']];
                        } //if
                        elseif (strpos($field["Type"], "int") !== false) {
                            $tableClass .= 'int $';
                            $setter[$field['Field']] = ['type' => 'int', 'null' => $field['Null']];
                        } //elseif
                        elseif (strpos($field["Type"], "tinyint") !== false) {
                            $tableClass .= 'boolean $';
                            $setter[$field["Field"]] = ['type' => 'boolean', 'null' => $field['Null']];
                        } //elseif
                        elseif (strpos($field["Type"], "date") !== false) {
                            $tableClass .= 'DateTime $';
                            $setter[$field["Field"]] = ['type' => 'DateTime', 'null' => $field['Null']];
                        }//elseif
                        $setter[$field['Field']] += ['Key' => ''];
                        $tableClass .= $field['Field'] . ';
                    ';
                    } elseif ($field['Key'] == "MUL") {

                        $parseKey = explode("_", $field['Field']);
                        if ($parseKey[0] === 'fk') {
                            $tableClass .= '
    private $' . $parseKey[1] . ';
';
                            $setter[$parseKey[1]] = ['type' => ucfirst($parseKey[1]), 'null' => $field['Null'], 'Key' => 'MUL', 'target' => ucfirst($parseKey[1])];
                        } //if
                    } //elseif
                } //foreach

                /*
                 * Gestion des relations manyToMany
                 */
                $attrsManyToManyClass = [];
                if (!empty($manyToManyFields)) {
                    foreach ($manyToManyFields as $tableRefName => $fieldsRef) {
                        $parseTablesNamesRef = explode("_", $tableRefName);
                        foreach ($parseTablesNamesRef as $parseTableNameRef) {
                            if ($tableName == $parseTableNameRef) {
                                foreach ($fieldsRef as $fieldRef) {
                                    $manyToManyfieldParse = explode("fk_", $fieldRef['Field']);
                                    if ($manyToManyfieldParse[1] != $tableName) {
                                        $tableClass .= '
    private Collection $' . $manyToManyfieldParse[1] . ';
';                                      $attrsManyToManyClass[] = $manyToManyfieldParse[1];
                                        $setter[$manyToManyfieldParse[1]] = ['type' => 'Collection', 'null' => $fieldRef['Null'], 'Key' => 'MUL', 'target' => ucfirst($manyToManyfieldParse[1])];

                                        break;
                                    } //if
                                } //foreach
                            } //if
                        } //foreach
                    } //foreach
                } //if

                /**
                 * Gestion des relations ManyToOne
                 */
                foreach ($tables as $tableName2 => $fieldsTable) {
                    if ($tableName != $tableName2) {
                        foreach ($fieldsTable as $fieldsForSearch) {
                            if ($fieldsForSearch['Key'] == 'MUL' && explode('_', $fieldsForSearch['Field'])[0] == 'fk') {
                                $fieldsForSearch = explode('_', $fieldsForSearch['Field'])[1];
                                if ($fieldsForSearch == $tableName && !array_key_exists('fk_' . $tableName2, $setter)) {
                                    $tableClass .= '
    private Collection $' . $tableName2 . ';' . "\n";
                                    $setter[$tableName2] = ['type' => 'Collection', 'null' => 'YES', 'Key' => 'MUL', 'target' => ucfirst($tableName2)];
                                    $attrsManyToManyClass[] = $tableName2;
                                }
                            }
                        }
                    }
                } //foreach

                //Génération du constructeur
                if (!empty($attrsManyToManyClass)) {
                    $tableClass .= '
                    
    public function __construct()
    {';
                    foreach($attrsManyToManyClass as $attrManyToManyClass) {
                       $tableClass .= '
        $this->' . $attrManyToManyClass . ' = new Collection(\'app\Repository\\' . ucfirst($attrManyToManyClass) . 'Repository\');';
                    } //foreach
                    $tableClass .= '
    }' . "\n";
                } //if

                //Génération des getter et setter
                foreach ($setter as $method => $attributs) {
                    $tableClass .= '
    public function get' . ucfirst($method) . '() : '. $attributs['type'] . '
    {
        return $this->' . $method . ';
    }
    
    public function set' . ucfirst($method) . '(';
                    if ($attributs['null'] === "YES") {
                        $tableClass .= '?';
                    } //if
                    if ($attributs['type'] == 'Collection') {
                        $tableClass .= $attributs['target'];
                    } else {
                        $tableClass .= $attributs['type'];
                    } //else
                    $tableClass .= ' $' . $method . ')
    {';
                    if ($attributs['type'] == 'Collection') {
                        $tableClass .= '
        $this->' . $method . '->add($' . $method . ');';
                    } else {
                        $tableClass .= '
        $this->' . $method . ' = $' . $method . ';';
                    }

        $tableClass .= '
        
        return $this;
    }
                    ';
                } //foreach

            $tableClass .= "
}";

                if (!is_dir("./back/app/Tables/")) {
                    mkdir("./back/app/Tables/");
                }

                file_put_contents("./back/app/Tables/" . ucfirst($tableName) . '.php', $tableClass);

                $repoClass = '<?php

namespace app\Repository;

use app\Tables\\' . ucfirst($tableName) . ';

class ' . ucfirst($tableName) . 'Repository extends \modelo\Repository\AbstractRepository
{
    private $' . $tableName . 'Table;

    public function __construct()
    {
        parent::__construct(' . ucfirst($tableName) . '::class);
    }
}
                ';

                if (!is_dir("./back/app/Repository/")) {
                    mkdir("./back/app/Repository/");
                }

                file_put_contents("./back/app/Repository/" . ucfirst($tableName) . 'Repository.php', $repoClass);



                echo
'Génération des fichiers :   back/app/Tables/' . ucfirst($tableName) . '.php
                             back/app/Repository/' . ucfirst($tableName) . 'Repository.php 

';
            } //foreach
        } //if
    } //Run
}