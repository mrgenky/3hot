<?php

namespace modelo\CMD;

class CMD
{
    private $cmd;

    public function __construct(array $listCMD)
    {
        $this->cmd = $listCMD;
    }

    public function run(String $cmdName, $args)
    {
        if (array_key_exists($cmdName, $this->cmd)) {
            $cmd = new $this->cmd[$cmdName]();
            if (isset($args) && !is_null($args)) {
                switch ($args) {
                    case 'help' :
                        $cmd->getHelp();
                        break;
                    default:
                        echo 'L\'argument donné "' . $args . ' n\'est pas valide !!';
                }
            } else {
                $cmd->run();
            }
        }
    }
}