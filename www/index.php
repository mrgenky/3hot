<?php

require 'back/autoload.php';
require 'back/modelo/ini.php';

use rotor\Schema;
use viewer\View;
use app\routes\MainSchema;

function basePath($uri, $paramsSeparator = '?') {
    $end = strrpos($uri, $paramsSeparator);
    if ($end === false)
        return $uri;

    return substr($uri, 0, $end);
}

try {
    \session_start();
    $schema = new MainSchema();
    Schema::route($schema, basePath($_SERVER['REQUEST_URI']), $_SERVER['REQUEST_METHOD']);
} catch (ValueError $e) {
    $schema->fallback();
} catch (\Error $e) {
    \http_response_code(500);
    \error_log($e->getMessage());
    echo View::fromFile('views/errors/500.php');
}
