<main class="container container-small">
    <header class="mb-4">
        <h1 class="text-center">Récupération du mot passe</h1>
    </header>

    <form method="post" action="">
        <p>
            <label for="inputEmail" class="form-label">Adresse mail</label>
            <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Saisir votre adresse mail" required>
        </p>
        <p class="alert alert-primary" role="alert">
            <i class="bi bi-info-circle-fill"></i> Un email vous sera envoyé avec les instructions à suivre.
        </p>
        <button type="submit" class="btn btn-primary">Envoyer</button>
        <a href="/login" role="button" class="btn btn-outline-secondary">Annuler</a>
    </form>
</main>