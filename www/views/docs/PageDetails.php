<main class="container">
    <header class="mb-4">
        <h1 class="text-center">Documentation</h1>
    </header>

    <section class="row">
        <?= $navigation ?>

        <article class="container-documentation col-12 col-lg-10 p-4">
            <h2>
                <?= \htmlspecialchars($page->getTitle()) ?>
            </h2>

            <p>
                <?= \htmlspecialchars($page->getContent()) ?>
            </p>
        </article>
    </section>
</main>
