<main class="container">
    <header class="mb-4">
        <h1 class="text-center">Documentation</h1>
    </header>

    <section class="row">

        <?= $navigation ?>

        <article class="container-documentation col-12 col-lg-10 p-4">
<?php foreach ($sections as $section): ?>
            <section class="section-documentation">
                <h2>
                    <?= $section->getName() ?>
                </h2>

    <?php if (!empty($section->getPage()->getAll())): ?>
                <ul>
         <?php foreach ($section->getPage()->getAll() as $page): ?>
                    <li>
                        <a href="/documentation/page/<?= $page->getID() ?>">
                            <?= $page->getTitle() ?>
                        </a>
                    </li>
         <?php endforeach; ?>
                </ul>
    <?php endif; ?>
            </section>
<?php endforeach; ?>
        </article>
    </section>
</main>