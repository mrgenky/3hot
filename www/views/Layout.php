<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>HotHotHot</title>
        <link rel= "manifest" href= "/manifest.webmanifest">
        <link rel="stylesheet" href="/index.css">
    </head>
</html>
<body>
    <nav class="navbar navbar-expand-sm navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="/"><i class="bi bi-house-door-fill"></i> HotHotHot</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <article class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                <ul class="navbar-nav links-navbar">
                    <li class="nav-item">
                        <a class="nav-link" href="/documentation">Documentation</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/login">Connexion</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/compte">Mon compte</a>
                    </li>
                </ul>
            </article>
        </div>
    </nav>

    <?= $body ?>

<?php if(!empty($error)): ?>
    <div class="col-4 container text-center alert alert-danger">
        <?= $error ?>
    </div>
<?php endif; ?>

<?php if(!empty($success)): ?>
    <div class="col-4 container text-center alert alert-success">
        <?= $success ?>
    </div>
<?php endif; ?>

    <script src="/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/node_modules/chart.js/dist/chart.min.js"></script>

<?php foreach($scripts ?? [] as $script): ?>
    <script type="module" src="/assets/js/<?= $script ?>"></script>
<?php endforeach; ?>

</body>
