<main class="container container-small">
    <header class="mb-4">
        <h1 class="text-center">Modification du mot de passe</h1>
    </header>

    <form method="post" action="" enctype="multipart/form-data">
        <p class="alert alert-success" role="alert">
            <i class="bi bi-check-circle-fill"></i> Un email vous a été envoyé avec les instructions à suivre.
        </p>
        <p class="alert alert-success" role="alert">
            <i class="bi bi-check-circle-fill"></i> Mot de passe changé avec succès.
        </p>
        <p>
            <label for="inputPassword" class="form-label">Nouveau mot de passe</label>
            <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Saisir le nouveau mot de passe" required>
        </p>
        <p>
            <label for="inputConfPassword" class="form-label">Confirmation du nouveau mot de passe</label>
            <input type="password" class="form-control" id="inputConfPassword" name="password-confirm" placeholder="Confirmer le nouveau mot de passe" required>
        </p>
        <button type="submit" class="btn btn-primary">Envoyer</button>
    </form>
</main>