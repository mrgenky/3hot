    <main class="container">
        <form method="post" action="">
            <p>
                <label for="inputUsername" class="form-label">Nom d'utilisateur</label>
                <input type="text" class="form-control" id="inputUsername" name="username" placeholder="Saisir le nom d'utilisateur" required>
            </p>
            <p>
                <label for="inputEmail" class="form-label">Adresse mail</label>
                <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Saisir l'adresse mail" required>
            </p>
            <p>
                <label for="inputPassword" class="form-label">Mot de passe</label>
                <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Saisir le mot de passe" required>
            </p>
            <p>
                <label for="inputConfPassword" class="form-label">Confirmation du mot de passe</label>
                <input type="password" class="form-control" id="inputConfPassword" name="password-confirm" placeholder="Confirmer le mot de passe" required>
            </p>
            <button type="submit" class="btn btn-primary">Ajouter l'utilisateur</button>
        </form>
    </main>