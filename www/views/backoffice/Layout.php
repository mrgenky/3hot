<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Back Office</title>
    <link rel="stylesheet" href="/index.css">
</head>
</html>
<body>
    <nav class="navbar navbar-dark bg-dark">
        <div class="container-fluid">
            <article>
                <a class="navbar-brand text-primary fw-bold" href="/admin">Back Office</a>
                <a class="text-white text-decoration-none" href="/">Front Office</a>
            </article>
            <!--<article class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Gestion Utilisateur</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="./AdminPage.html">Gestion Page</a>
                    </li>
                </ul>
            </article>-->
        </div>
    </nav>
    <header class="container mt-3 mb-4">
        <h1><?= $h1 ?></h1>
    </header>

    <?= $body ?>

    <?php if(!empty($error)): ?>
        <div class="col-4 container text-center alert alert-danger">
            <?= $error ?>
        </div>
    <?php endif; ?>

    <?php if(!empty($success)): ?>
        <div class="col-4 container text-center alert alert-success">
            <?= $success ?>
        </div>
    <?php endif; ?>
    <script src="/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script>
        //Activer les tooltips de Bootstrap
        document.addEventListener("DOMContentLoaded", function() {
            var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
            var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
                return new bootstrap.Tooltip(tooltipTriggerEl)
            })
        });
    </script>
</body>
