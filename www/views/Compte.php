<main class="container container-small">
    <header class="mb-4">
        <h1 class="text-center">Mon compte</h1>
    </header>
    <form>
        <p>
            <label for="inputUsername" class="form-label">Nom d'utilisateur</label>
            <input type="text" class="form-control" id="inputUsername" placeholder="jean.dupont" readonly>
        <p>
        <p>
            <label for="inputEmail" class="form-label">Adresse mail</label>
            <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="jean.dupont@gmail.com" readonly>
        </p>
        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modalConfirmation"><i class="bi bi-exclamation-triangle-fill"></i> Supprimer</button>
        <a href="/logout" class="btn btn-outline-danger">Déconnexion</a>
    </form>

    <!-- Modal -->
    <div class="modal fade" id="modalConfirmation" tabindex="-1" aria-labelledby="modalConfirmation" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalConfirmationLabel">Confirmation</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>
                        Voulez-vous vraiment supprimer votre compte ?
                    </p>
                    <p class="alert alert-danger" role="alert">
                        <i class="bi bi-info-circle-fill"></i> L'opération est irréversible.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Annuler</button>
                    <button type="button" class="btn btn-danger">Confirmer</button>
                </div>
            </div>
        </div>
    </div>
</main>