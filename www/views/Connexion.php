<main class="container container-small">
    <header class="mb-4">
        <h1 class="text-center">Connexion</h1>
        <p class="text-center fw-light">Pas encore inscrit ? <a href="/signup">Inscription</a></p>
    </header>

<?php if (!empty($error)): ?>
    <p class="card alert-danger p-3">
        <?= $error; ?>
    </p>
<?php endif; ?>

    <form method="POST" action="/login" enctype="application/x-www-form-urlencoded">
        <p>
            <label for="inputUsername" class="form-label">Nom d'utilisateur</label>
            <input type="text" class="form-control" id="inputUsername" placeholder="Saisir votre nom d'utilisateur" name="username" required>
        <p>
        <p>
            <label for="inputPassword" class="form-label">Mot de passe</label>
            <input type="password" class="form-control" id="inputPassword" placeholder="Saisir votre mot de passe" name="password" required>
            <a href="/recover-account/" class="txt-oubli-mdp">Mot de passe oublié ?</a>
        <p>
        <button type="submit" class="btn btn-primary">Se connecter</button>
    </form>
    <p class="divider-formulaire"><span>Ou</span></p>
    <button class="btn btn-secondary btn-google">
        <i class="bi bi-google"></i> Connectez-vous avec Google
    </button>
</main>