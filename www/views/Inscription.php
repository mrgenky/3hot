<main class="container container-small">
    <header class="mb-4">
        <h1 class="text-center">Inscription</h1>
        <p class="text-center fw-light">Déjà membre ? <a href="/login">Connexion</a></p>
    </header>

    <?php if (!empty($error)): ?>
        <p class="card alert-danger p-3">
            <?= $error; ?>
        </p>
    <?php endif; ?>

    <!--    TODO verifier tous les attributs qu'on peut mettre aux inputs + regles validation-->
    <form method="post" action="/signup" enctype="multipart/form-data">
        <p>
            <label for="inputUsername" class="form-label">Nom d'utilisateur</label>
            <input type="text" class="form-control" id="inputUsername" name="username" placeholder="Saisir votre nom d'utilisateur" required>
        </p>
        <p>
            <label for="inputEmail" class="form-label">Adresse mail</label>
            <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Saisir votre adresse mail" required>
        </p>
        <p>
            <label for="inputPassword" class="form-label">Mot de passe</label>
            <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Saisir votre mot de passe" required>
        </p>
        <p>
            <label for="inputConfPassword" class="form-label">Confirmation du mot de passe</label>
            <input type="password" class="form-control" id="inputConfPassword" name="password-confirm" placeholder="Confirmer votre mot de passe" required>
        </p>
        <button type="submit" class="btn btn-primary">S'inscrire</button>
    </form>
    <p class="divider-formulaire"><span>Ou</span></p>
    <button class="btn btn-secondary btn-google">
        <i class="bi bi-google"></i> Connectez-vous avec Google
    </button>
</main>