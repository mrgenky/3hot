# 3hot

## Get started

Execute services in detached mode:
`docker-compose -f docker-compose.yml docker-compose.dev.yml up -d`

Connect to a TTY:
`docker exec -ti <CONTAINER-ID> /bin/bash`

Build styles:
- `npm run build` builds css once
- `npm run dev` builds css on change


## Configuring mailing

Copy `./etc/msmtprc.template` into `./etc/msmtprc`.

Replace `MAIL_HOST`, `MAIL_PORT`, `MAIL_USER`, `MAIL_PASSWORD` by real values. Don't quote the values.

Read more about configuring `msmtprc` : https://doc.ubuntu-fr.org/msmtp#configuration

Be sure to enable POP3 and IMAP on the mail host.

To test, execute `echo 'message' | your@mail.com` on the `web_server` container.


## Deploying

Be sure to read section about configuring mailing.

Run `npm run build` to build styles.

Run `docker-compose build && docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d`.

Wait about a minute for the database to initialize.
You can have a look at the logs with `docker logs -f <CONTAINER_ID>`

When the database is ready, run `docker exec -ti`, then `modelo orm:database-import`.

> Note that there is no bound volumes between code and the containers in  production mode.
